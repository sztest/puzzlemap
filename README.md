## Puzzle/Adventure Map System using Node Protection Mechanic

*Compatible/tested with NodeCore, but probably/maybe works with a lot of other Minetest games*

This mod aids in the creation of maps for adventure, puzzle, escape room, etc play modes.  Most nodes are protected from being changed, but the map author can exempt specific nodes/locations that the player is supposed to interact with to solve the puzzle.

### How To Use:

- Grant yourself `protection_bypass` to allow full access to map.  Grant any other privs necessary to access the tools you need to build.

- Build the puzzle map.

- Grant yourself the `puzzlemap` priv to gain access to the puzzlemap tools.

- Make room in your inventory, then use the `/puzzlemaptools` command to summon the set of tools for managing puzzle map acess.
  
- Punch a node with the "by Position" tool to allow access to just that specific location.  Players will be able to dig/place anything at that location.

- Punch a node with the "by Node Type" tool to allow access to all nodes of a specific type.  Players will be able to dig/place that type of node anywhere.

- Punch a node with the "by Exact node" tool to allow access only to a specific node at that specific location.  Players will only be allowed to interact with that exact node at that location; if it is removed/changed they will not be allowed anymore (until/unless the map resets it).

- Sneak+punch with any of these tools will apply the rule to the space above the surface you're punching, including air/liquids.

- Punch things again to toggle access back off. Check the totals to make sure they match expectations.

- You will be able to see markings surrounding nodes for which access has been granted.  Note that for some, particularly the "exact" type, you will only see the markings if the node is *currently* accessible based on the rule, e.g. there may be "exact" match types that are not currently visible because the node currently in that position does not match.
  
- If you screw up and want to start over again, `/puzzlemapreset`.  Note that this will remove *all* access and require you to re-permit everything from scratch, and there is no confirmation on reset.

- Make use of fly, noclip, etc. to bypass your mechanisms so you can leave them in a ready-to-play state.

- When finished, drop the tools (they will dematerialize automatically) and `/revoke` yourself the protection_bypass, fly, noclip, etc. privs.

- Make a backup copy of the map, so you don't have to reset all mechanisms manually.

- Play-test the map as an ordinary player.  Remember to check to make sure each thing you should be able to access works, *and* spot-check to ensure things you're not supposed to be able to access block you.
