-- LUALOCALS < ---------------------------------------------------------
local minetest, rawget, rawset, setmetatable
    = minetest, rawget, rawset, setmetatable
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

local modstore = minetest.get_mod_storage()

function api.loaddb(db)
	db = db and db ~= "" and minetest.deserialize(db) or {}
	setmetatable(db, {
			__index = function(_, k)
				local t = {}
				db[k] = t
				return t
			end
		})
	api.db = db
	return db
end
api.loaddb(modstore:get_string("db"))

function api.savedb()
	modstore:set_string("db", minetest.serialize(api.db))
end

local function match(pos, node)
	local poskey = minetest.pos_to_string(pos)
	local nodename = node and node.name or minetest.get_node(pos).name
	if api.db.exact[poskey] == nodename then return "exact" end
	if api.db.pos[poskey] then return "pos" end
	if api.db.node[nodename] then return "node" end
end
api.match = match

local oldprot = minetest.is_protected
function minetest.is_protected(pos, name, ...)
	if minetest.check_player_privs(name, "protection_bypass") then
		return oldprot(pos, name, ...)
	end
	return (not match(pos)) or oldprot(pos, name, ...)
end
