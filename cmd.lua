-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget
    = minetest, pairs, rawget
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname)

minetest.register_privilege(modname, {
		description = "access to " .. modname .. " tools and commands",
		give_to_admin = false,
		give_to_singleplayer = false
	})

minetest.register_chatcommand(modname .. "reset", {
		description = "reset all puzzle map access",
		privs = {[modname] = true},
		func = function()
			api.loaddb()
			api.savedb()
			return true, "database reset"
		end
	})

minetest.register_chatcommand(modname .. "tools", {
		description = "summon all puzzle map tools",
		privs = {[modname] = true},
		func = function(name)
			local player = minetest.get_player_by_name(name)
			if not player then return false, "must be online to use" end
			local inv = player:get_inventory()
			local got = {}
			for i = 1, inv:get_size("main") do
				got[inv:get_stack("main", i):get_name()] = true
			end
			for k in pairs(api.tools or {}) do
				if not got[k] then inv:add_item("main", k) end
			end
		end
	})
