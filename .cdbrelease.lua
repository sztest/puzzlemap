-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

return {
	user = "Warr1024",
	pkg = "puzzlemap",
	dev_state = "MAINTENANCE_ONLY",
	version = stamp .. "-$Format:%h$",
	type = "mod",
	long_description = readtext('README.md'),
	screenshots = {readbinary('.cdbscreen.webp')}
}

-- luacheck: pop
