-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget, vector
    = minetest, pairs, rawget, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname)

local zero = {x = 0, y = 0, z = 0}
local pr = 0.55
local function showfx(x, y, z, txr, pname)
	for _, pos in pairs({
			{x = x, y = y, z = z},
			{x = x - pr, y = y, z = z},
			{x = x + pr, y = y, z = z},
			{x = x, y = y - pr, z = z},
			{x = x, y = y + pr, z = z},
			{x = x, y = y, z = z - pr},
			{x = x, y = y, z = z + pr}
		}) do
		minetest.add_particle({
				pos = pos,
				velocity = zero,
				acceleration = zero,
				expirationtime = 1.25,
				glow = 14,
				size = 1,
				texture = txr,
				playername = pname
			})
	end
end

local radius = 5
local function doplayer(player)
	local pos = player:get_pos()
	pos.y = pos.y + player:get_properties().eye_height
	pos = vector.round(pos)
	local match = api.match
	local pname = player:get_player_name()
	for z = pos.z - radius, pos.z + radius do
		for y = pos.y - radius, pos.y + radius do
			for x = pos.x - radius, pos.x + radius do
				local m = match({x = x, y = y, z = z})
				local tool = m and minetest.registered_items[modname .. ":tool_" .. m]
				if tool then showfx(x, y, z, tool.inventory_image, pname) end
			end
		end
	end
end

local function timer()
	minetest.after(1, timer)
	for _, player in pairs(minetest.get_connected_players()) do
		if minetest.check_player_privs(player, modname) then
			doplayer(player)
		end
	end
end
minetest.after(0, timer)
