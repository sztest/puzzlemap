-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget, tostring, type
    = minetest, pairs, rawget, tostring, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname)

local function mktooluse(dbset, dbkey, dbval, desc)
	return function(_, player, pointed)
		if not (player and player:is_player() and pointed.type == "node"
			and pointed.above and pointed.under) then return end
		local set = api.db[dbset]
		local pos = player:get_player_control().sneak and pointed.above or pointed.under
		local key = dbkey(pos)
		if set[key] then
			set[key] = nil
		else
			set[key] = type(dbval) == "function" and dbval(pos) or dbval
		end
		api.savedb()
		local str = desc .. " " .. key .. " = "
		.. tostring(set[key] or "blocked") .. " (total"
		for sk, sv in pairs(api.db) do
			local total = 0
			for _ in pairs(sv) do total = total + 1 end
			str = str .. " " .. sk .. "=" .. total
		end
		return minetest.chat_send_player(player:get_player_name(), str .. ")")
	end
end

local function toolimg(color)
	local base = "{[combine:1x1&[noalpha&[colorize:#" .. color .. ":255"
	return "[inventorycube" .. base .. base .. base
end

local function mktool(name, desc, color, dbkey, dbval)
	api.tools = api.tools or {}
	local toolname = modname .. ":tool_" .. name
	api.tools[toolname] = true
	minetest.register_tool(toolname, {
			description = "Toggle Access by " .. desc,
			inventory_image = toolimg(color),
			virtual_item = true,
			on_drop = function() return "" end,
			on_use = mktooluse(name, dbkey, dbval, desc)
		})
end

mktool("pos", "Position", "0080FF",
	minetest.pos_to_string,
	true)

mktool("node", "Node Type", "8000FF",
	function(p) return minetest.get_node(p).name end,
	true)

mktool("exact", "Exact Node", "FF8040",
	minetest.pos_to_string,
	function(p) return minetest.get_node(p).name end)
