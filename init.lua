-- LUALOCALS < ---------------------------------------------------------
local dofile, minetest
    = dofile, minetest
-- LUALOCALS > ---------------------------------------------------------

local function inc(name)
	return dofile(minetest.get_modpath(minetest.get_current_modname())
		.. "/" .. name .. ".lua")
end

inc("api")
inc("tools")
inc("cmd")
inc("visual")
